import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';


import { BehaviorSubject } from 'rxjs';
import { Activity } from '../entity/activity';
import { ActivityTableDataSource } from './activity-table-datasource';
import { AppService } from '../service/app-service';
import { StudentService } from '../service/student-service';

@Component({
  selector: 'app-activity-table',
  templateUrl: './activity-table.component.html',
  styleUrls: ['./activity-table.component.css']
})
export class ActivityTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ActivityTableDataSource;


  displayedColumns = [];
  activities: any[];
  filter: string;
  filter$: BehaviorSubject<string>;
  disableread: boolean[] = [];
  currentUser: any;
  constructor(private appService: AppService, private studentService: StudentService) {

  }
  ngOnInit() {


    if (localStorage.getItem('admin')) {
      this.displayedColumns = ['name', 'description', 'location', 'host', 'period', 'date', 'students'];
      this.currentUser = localStorage.getItem('admin');
    } else if (localStorage.getItem('student')) {
      this.displayedColumns = ['name', 'description', 'location', 'host', 'period', 'date', 'enroll'];
      this.currentUser = JSON.parse(localStorage.getItem('student'));
    } else if (localStorage.getItem('teacher')) {
      this.displayedColumns = ['name', 'description', 'location', 'host', 'period', 'date', 'students'];
      this.currentUser = JSON.parse(localStorage.getItem('teacher'));
    }

    this.appService.getActivities().subscribe(data => {
      this.activities = data.map(e => {
        return {
          date: e.payload.doc.data()['date'],
          description: e.payload.doc.data()['description'],
          host: e.payload.doc.data()['host'],
          location: e.payload.doc.data()['location'],
          name: e.payload.doc.data()['name'],
          period: e.payload.doc.data()['period'],
          students: e.payload.doc.data()['students'],
          id: e.payload.doc.id
        };
      })

      this.dataSource = new ActivityTableDataSource();
      this.dataSource.data = this.activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      for (let i = 0; i < this.activities.length; i++) {
        this.disableread[i] = false;
      }
    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  enroll(activity, index) {
    this.disableread[index] = !this.disableread[index];
    
    if (this.currentUser.activities.indexOf(activity.id) != -1) {
      alert('You already enrolled this activity!');
    }
    this.studentService.enroll(activity.name, (this.currentUser.studentId));
  }
}
