import { Injectable } from '@angular/core'
import { AngularFirestore } from '@angular/fire/firestore';



@Injectable({
    providedIn: 'root'
})

export class AppService {
    constructor(private firestore: AngularFirestore) {}

    getStudents() {
        return this.firestore.collection('students').snapshotChanges();
    }

    getTeachers() {
        return this.firestore.collection('teachers').snapshotChanges();
    }

    getActivities(){
        return this.firestore.collection('activity').snapshotChanges();
    }

    getWaiting(){
        return this.firestore.collection('waiting').snapshotChanges();
    }

    confirm(student: string){
        return this.firestore.doc('waiting/'+ student).delete();
    }
    
    addStudent(record){
        return this.firestore.collection("students").add(record);
    }

    addActivity(record){
        return this.firestore.collection("activity").add(record);
    }

}