import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CustomMaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { AdminComponent } from './admin/admin.component';
import { TeacherComponent } from './teacher/teacher.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddComponent } from './add/add.component';
import { RegisterComponent } from './register/register.component';
import { MatTabsModule } from '@angular/material/tabs'
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import FirebaseConfig from '../firebase';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { MatSelectModule } from '@angular/material/select';
import { AppService } from './service/app-service';
import { StudentNavComponent } from './student-nav/student-nav.component';
import { WaitingComponent } from './waiting/waiting.component';
import { EnrolledComponent } from './enrolled/enrolled.component';
import { ActivityTableComponent } from './activity-table/activity-table.component';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule
  , MatIconModule, MatListModule, MatGridListModule, MatCardModule
  , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule
} from '@angular/material';
import { TeacherNavComponent } from './teacher-nav/teacher-nav.component';
import { TeacherService } from './service/teacher-service';
import { ConfirmationTableComponent } from './confirmation-table/confirmation-table.component';
import { MyActivityTableComponent } from './my-activity-table/my-activity-table.component';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { EditComponent } from './edit/edit.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './service/auth.service';
import { HttpClientModule } from '@angular/common/http'; 
import { ProfileComponent } from './profile/profile.component';
import { MatFileUploadModule } from 'mat-file-upload';
 
@NgModule({

  declarations: [
    AppComponent,
    StudentComponent,
    AdminComponent,
    TeacherComponent,
    AdminNavComponent,
    RegistrationComponent,
    AddComponent,
    LoginComponent,
    RegisterComponent,
    StudentNavComponent,
    WaitingComponent,
    EnrolledComponent,
    ActivityTableComponent,
    TeacherNavComponent,
    ConfirmationTableComponent,
    MyActivityTableComponent,
    EditComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    FormsModule,
    CustomMaterialModule,
    AppRoutingModule,
    MatTabsModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(FirebaseConfig),
    AngularFirestoreModule,
    MatSelectModule,
    MatToolbarModule, MatButtonModule, MatSidenavModule
    , MatIconModule, MatListModule, MatGridListModule, MatCardModule
    , MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule,
    MatDatepickerModule, MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatFileUploadModule
  ],
  providers: [AngularFirestore, AppService, TeacherService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
