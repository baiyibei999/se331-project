import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  user: any;
  name: any;
  role: any;
  url: any;
  constructor(private router: Router, private auth: AuthService) {

  }

  ngOnInit() {
    if (localStorage.getItem('token') == null) {
      this.router.navigate(['/login']);
      alert('Please login');
    } else {
      this.user = JSON.parse(localStorage.getItem("currentUser"));
      this.name = this.user.surname + ' ' + this.user.name;
      this.role = this.user.authorities;
      this.url = this.user.image;
    }

  }
  logout() {
    this.auth.logout();
    this.router.navigate(["/login"]);
  }
}
