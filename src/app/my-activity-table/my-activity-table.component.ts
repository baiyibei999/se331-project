import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BehaviorSubject } from 'rxjs';
import { Activity } from '../entity/activity';
import { MyActivityTableDataSource } from './my-activity-table-datasource';
import { TeacherService } from '../service/teacher-service';
import { AppService } from '../service/app-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-activity-table',
  templateUrl: './my-activity-table.component.html',
  styleUrls: ['./my-activity-table.component.css']
})
export class MyActivityTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: MyActivityTableDataSource;
  displayedColumns = ['name', 'description', 'location', 'host', 'period', 'date', 'students', 'edit'];
  activities: any[];
  as: Array<any> = [];
  filter: string;
  filter$: BehaviorSubject<string>;
  disabled: boolean[] = [];
  currentUser: any;
  isEdit: boolean;

  editId: string;
  name: string;
  location: string;
  description: string;
  date: any;
  period: any;
  host: string;
  students: string[]

  constructor(private teacherService: TeacherService, private appService: AppService) {

  }
  ngOnInit() {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.appService.getActivities().subscribe(data => {
      this.activities = data.map(e => {

        return {
          date: e.payload.doc.data()['date'],
          description: e.payload.doc.data()['description'],
          host: e.payload.doc.data()['host'],
          location: e.payload.doc.data()['location'],
          name: e.payload.doc.data()['name'],
          period: e.payload.doc.data()['period'],
          students: e.payload.doc.data()['students'],
          id: e.payload.doc.id
        };
      });

      this.dataSource = new MyActivityTableDataSource();
      this.dataSource.data = this.as;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;


      for (let i = 0; i < this.activities.length; i++) {
        if (this.activities[i].host == this.currentUser.surname + ' ' + this.currentUser.name) {
          this.as.push(this.activities[i]);
        }
      }

      for (let i = 0; i < this.as.length; i++) {
        this.disabled[i] = false;
      }

    });

  }

  ngAfterViewInit() {
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  enroll(activity, index) {
    this.disabled[index] = !this.disabled[index]
    console.log(activity);
  }

  edit(id) {
    this.isEdit = !this.isEdit;
    this.editId = id;
    console.log(this.editId);
    for (let i = 0; i < this.as.length; i++) {
      if (id = this.as[i].id) {
        this.name = this.as[i].name;
        this.location = this.as[i].location;
        this.description = this.as[i].description;
        this.date = this.as[i].date;
        this.period = this.as[i].period;
        this.host = this.as[i].host;
        this.students = this.as[i].students;
      }
    }

  }

  save() {
    let record = {};
    record['name'] = this.name;
    record['location'] = this.location;
    record['description'] = this.description;
    record['period'] = this.period;
    record['host'] = this.host;
    record['date'] = this.date;
    record['students'] = this.students;

    this.teacherService.update(record, this.editId).then(resp => {
      this.name = "";
      this.location = "";
      this.description = "";
      this.period = "";
      this.date = "";
      this.host = "";

      this.isEdit = false;
      window.location.reload();
      alert('Updating Successful!');
    })
      .catch(error => {
        console.log(error);
      });

  }


}
