import { Component, OnInit } from '@angular/core';
import { FileService } from '../service/file.service';
import { HttpEventType, HttpEvent } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  uploadedUrl: string;
  progress:number;
  identify: string;
  image: string;
  constructor(private fileUploadService:FileService) { }
  onUploadClicked(files?: FileList) {
    console.log(typeof(files));
    console.log(files.item(0));
    const uploadedFile = files.item(0);
    this.progress = 0;
    this.fileUploadService.uploadFile(uploadedFile)
      .subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            console.log('Response header has been received!');
            break;
          case HttpEventType.UploadProgress:
            this.progress = Math.round(event.loaded / event.total * 100);
            console.log(`Uploaded! ${this.progress}%`);
            break;
          case HttpEventType.Response:
            console.log('User successfully created!', event.body);
            this.uploadedUrl = event.body;
            this.image= this.uploadedUrl;
            setTimeout(() => {
              this.progress = 0;
            }, 1500);
          }
        });
  }
  ngOnInit() {
  }
  onSelectedFilesChanged(files?: FileList){

  }
  
}
